package com.example.student1.a2screens;



import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    Button button;
    EditText login;
    EditText password;
    EditText repassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.pas);
        repassword = (EditText) findViewById(R.id.rep);

        button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(this);



    }

    public void onClick(View view) {

        String username = login.getText().toString();

        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("login1", username);

        startActivity(intent);


    }
}
