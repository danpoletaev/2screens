package com.example.student1.a2screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {


    TextView hello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        hello = (TextView) findViewById(R.id.hello1);

        Intent intent = getIntent();
        String username = intent.getExtras().getString("login");

        hello.setText("Привет, " + username);

    }
}
